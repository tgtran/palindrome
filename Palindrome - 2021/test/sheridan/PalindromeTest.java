package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean isValidPalindrome = Palindrome.isPalindrome("anna"); 
		assertTrue( "Word is not a palindrome", isValidPalindrome == true );
	}

	@Test
	public void testIsPalindromeNegative() {
		boolean isValidPalindrome = Palindrome.isPalindrome("ann"); 
		assertTrue( "Word is not a palindrome", isValidPalindrome == false);
	}
	
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean isValidPalindrome = Palindrome.isPalindrome("a");
		assertTrue( "Word is not a palindrome", isValidPalindrome == true );
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean isValidPalindrome = Palindrome.isPalindrome("dfsannasfd");
		assertTrue( "Word is not a palindrome", isValidPalindrome == true );
	}	
	
}
